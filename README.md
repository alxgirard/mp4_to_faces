# Requirements

* [deepdetect-js](https://github.com/jolibrain/deepdetect-js)
* [gm](https://github.com/aheckmann/gm)

```
yarn install
```

# Usage

```bash
# extract frames from mp4 file
./frames.sh
# use deepdetect object detection service
# to extract cropped image to detect folder
node index.js
```
