# https://www.bugcodemaster.com/article/extract-images-frame-frame-video-file-using-ffmpeg

## Extract all keyframes from a video file
ffmpeg -i ./*.mp4 -vf "select=eq(pict_type\,I)" -vsync vfr thumb%04d.jpg -hide_banner
