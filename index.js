var DD = require('deepdetect-js'),
  gm = require('gm').subClass({imageMagick: true}),
  fs = require('fs'),
  path = require('path');

const dd = new DD({
  host: '10.10.77.61',
  port: 18104,
  path: '/api/private'
})

async function run(fileNames) {
  const postData = {
    "service": "detect",
    "parameters": {
      "input": {},
      "output": {
        "confidence_threshold": 0.5,
        "bbox": true
      },
      "mllib": {
        "gpu": true
      }
    },
    "data": fileNames.map(name => `/opt/platform/data/detect_alx/${name}`)
  }
  const predict = await dd.postPredict(postData);

  if(predict.body && predict.body.predictions) {
    predict.body.predictions.forEach( prediction => {
      console.log(prediction.classes.map(c => c.cat));
      const uri = prediction.uri;
      prediction.classes.forEach( c => {
        const { xmin, xmax, ymin, ymax } = c.bbox;

        var dir = `./detect/${c.cat}`;
        if (!fs.existsSync(dir)){
          fs.mkdirSync(dir);
        }

        gm(uri.replace('/opt/platform/data/detect_alx', './frames'))
          .crop(parseInt(xmax - xmin), parseInt(ymin - ymax), parseInt(xmin), parseInt(ymax))
          .write(`${dir}/${path.basename(uri)}`, (err) => {
            if (err) console.log(err);
          })

      });
    });
  }
}

fs.readdir('./frames', (err, files) => {
  var i,j,temparray,chunk = 10;
  for (i=0,j=files.length; i<j; i+=chunk) {
    run(files.slice(i,i+chunk))
  }
})
